# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import product_limit


def register():
    Pool.register(
        product_limit.ProductLimit,
        product_limit.ShipmentOut,
        product_limit.ShipmentOutReturn,
        product_limit.Location,
        product_limit.PrintProductLimitNoteParam,
        product_limit.Configuration,
        product_limit.ShipmentInternal,
        module='stock_location_product_limit', type_='model')
    Pool.register(
        product_limit.PrintProductLimitNote,
        module='stock_location_product_limit', type_='wizard')
    Pool.register(
        product_limit.ProductLimitNote,
        product_limit.DeliveryNote,
        product_limit.RestockingList,
        product_limit.InternalReport,
        module='stock_location_product_limit', type_='report')
