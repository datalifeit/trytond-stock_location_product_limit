======================
Product Limit Scenario
======================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock_location_product_limit Module::

    >>> config = activate_modules('stock_location_product_limit')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> view_loc = Location()
    >>> view_loc.name = 'View location'
    >>> view_loc.type = 'view'
    >>> view_loc.save()

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> add_warehouse = Wizard('party.party.create_warehouse', [customer])
    >>> add_warehouse.form.warehouse_per_party = True
    >>> add_warehouse.form.parent = view_loc
    >>> add_warehouse.execute('add_')

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product = template.products[0]


Create Product Limit::

    >>> ProductLimit = Model.get('stock.location.product_limit')
    >>> pl = ProductLimit()
    >>> pl.location = customer.warehouse
    >>> pl.product = product
    >>> pl.quantity = 0
    >>> pl.uom = unit
    >>> pl.save()

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Add two shipment lines of same product::

    >>> StockMove = Model.get('stock.move')
    >>> shipment_out.outgoing_moves.extend([StockMove(), StockMove()])
    >>> for move in shipment_out.outgoing_moves:
    ...     move.product = product
    ...     move.uom = unit
    ...     move.quantity = 1
    ...     move.from_location = output_loc
    ...     move.to_location = customer_loc
    ...     move.company = company
    ...     move.unit_price = Decimal('1')
    ...     move.currency = company.currency
    >>> shipment_out.save()

Set the shipment state to waiting::

    >>> shipment_out.click('wait')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: The limit of the product Product is exceeded. - 
    >>> len(shipment_out.outgoing_moves)
    2
    >>> len(shipment_out.inventory_moves)
    0

Set the shipment state to draft::

    >>> shipment_out.click('draft')

Set the product limit to 0::

    >>> pl.quantity = 1
    >>> pl.save()

Set the shipment state to waiting::

    >>> shipment_out.click('wait')
    >>> shipment_out.click('assign_try')
    False
    >>> shipment_out.click('pack')
    >>> shipment_out.click('done')

Testing the reports::

    >>> delivery_note = Report('stock.shipment.out.delivery_note')
    >>> ext, _, _, name = delivery_note.execute([shipment_out], {})
    >>> ext
    'odt'
    >>> name.split('-')[0]
    'Delivery Note'
    >>> conf = Model.get('stock.configuration')(1)
    >>> conf.show_limit = False
    >>> conf.save()
    >>> delivery_note = Report('stock.shipment.out.delivery_note')
    >>> ext, _, _, name = delivery_note.execute([shipment_out], {})

Testing note print wizard::

    >>> limit_note = Wizard('stock.location.product_limit.note_print')
    >>> limit_note.form.start_date = yesterday
    >>> limit_note.form.end_date = today
    >>> limit_note.form.location = warehouse_loc
    >>> limit_note.form.product = product
    >>> limit_note.execute('print_')


Create Shipment Internal::

    >>> ShipmentInternal = Model.get('stock.shipment.internal')
    >>> shipment_internal = ShipmentInternal()
    >>> shipment_internal.party = customer
    >>> shipment_internal.planned_date = today
    >>> shipment_internal.company = company
    >>> shipment_internal.from_location = storage_loc
    >>> shipment_internal.to_location = customer.warehouse.storage_location
    >>> move = shipment_internal.moves.new()
    >>> move.from_location = storage_loc
    >>> move.to_location = customer.warehouse.storage_location
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 2
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_internal.click('wait')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: The limit of the product Product is exceeded. - 
    >>> pl.quantity = 10
    >>> pl.save()
    >>> shipment_internal.click('wait')